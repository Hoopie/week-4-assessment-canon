#include <assert.h>

#include "Game.h"
#include "Application.h"

using namespace std;

/******************************************************************
game implementation code
Note that we can't give mPMode and mNMode constructors, for example: PlayMode(Game&)
which seems obvious and elegant, because the Game object itself wouldn't have finished
constructing, so if in one of those mode objects, it started using the Game& reference,
that would be bad as the Game object might not be completely ready (hence Initialise(Game*)
functions instead). We will learn how to get around that using dynamic allocation later.
*/
Game::Game()
	: mState(StateMachine::WAITING_INIT)
{}

void Game::Initialise(sf::RenderWindow& window)
{
	assert(mState == StateMachine::WAITING_INIT);
	mGD.pWindow = &window;
	//load the font
	if (!mGD.font.loadFromFile("data/fonts/comic.ttf"))
		assert(false);

	//setup
	mNMode.Init(this);
	mPMode.Init(this);
	mState = StateMachine::GET_NAME;
}

void Game::Update()
{
	switch (mState)
	{
	case StateMachine::GET_NAME:
		mNMode.Update();
		break;
	case StateMachine::PLAY:
		mPMode.Update();
		break;
	}
}

void Game::Render()
{
	switch (mState)
	{
	case StateMachine::GET_NAME:
		mNMode.Render();
		break;
	case StateMachine::PLAY:
		mPMode.Render();
		break;
	}

}

void Game::TextEntered(char key)
{
	//escape overrides everything else, just quit
	switch (key)
	{
	case GDC::ESCAPE_KEY:
		mGD.pWindow->close();
		break;
	}

	//pass input messages to different mode objects
	switch (mState)
	{
	case StateMachine::GET_NAME:
		mNMode.TextEntered(key);
		break;
	case StateMachine::PLAY:
		break;
	default:
		assert(false);
	}
}

void NameMode::Init(Game *pG)
{
	assert(!mpGame);
	mpGame = pG;
}






//******************************************************************
//a mini game frontend
void NameMode::TextEntered(char key)
{
	assert(mpGame);
	string& nm = mpGame->GetData().playerName;
	if (key == GDC::ENTER_KEY && nm.length() > 0)
	{
		mpGame->SetState(Game::StateMachine::PLAY);
	}
	else
	{
		if (isalpha(key))
			nm += key;
	}
}

void NameMode::Render()
{
	assert(mpGame);
	string mssg = "Type your name (press enter when complete): ";
	mssg += mpGame->GetData().playerName;
	sf::Text txt(mssg, mpGame->GetData().font, 30);
	txt.setPosition(100, 100);
	mpGame->GetData().pWindow->draw(txt);
}

//******************************************************************
//a mini playable game (sort of)
void PlayMode::Init(Game *pG)
{
	mpGame = pG;
	if (!mCannonTex.loadFromFile("data/cannon.png"))
		assert(false);
	mCannonTex.setSmooth(true); //it will look gnarly when it turns otherwise, try it. Probably using anisotropic sub pixel sampling with blur (look it up)
	mGun.SetTexture(mCannonTex, sf::IntRect(0, 0, 156, 138));
	mGun.SetOrigin(Vec2f{ 69, 73 });

	if (!mWallTex.loadFromFile("data/tiledwall.png"))
		assert(false);
	mWallTex.setRepeated(true);
	mWallTex.setSmooth(true);

	//resolution independant with the sprite scaled up/down as necessary
	//the texture is set to wrap and uses a 'tilable' brick pattern so it can grow to any size
	mWalls[Wall::TOP].SetTexture(mWallTex, sf::IntRect{ 0,0,(int)(GDC::SCREEN_RES.x*(1-GDC::WALL_WDITH*2)), (int)(GDC::SCREEN_RES.y*GDC::WALL_WDITH) });
	mWalls[Wall::TOP].SetPos(Vec2f{ GDC::SCREEN_RES.x*GDC::WALL_WDITH,0 });
	mWalls[Wall::TOP].SetOrigin(Vec2f{ 0,0 });
	mWalls[Wall::TOP].Init(Wall::TOP);

	mWalls[Wall::BOTTOM].SetTexture(mWallTex, sf::IntRect{ 0,0,(int)(GDC::SCREEN_RES.x*(1-GDC::WALL_WDITH*2)), (int)(GDC::SCREEN_RES.y*GDC::WALL_WDITH) });
	mWalls[Wall::BOTTOM].SetPos(Vec2f{ GDC::SCREEN_RES.x*GDC::WALL_WDITH,GDC::SCREEN_RES.y*(1- GDC::WALL_WDITH) });
	mWalls[Wall::BOTTOM].SetOrigin(Vec2f{ 0,0 });
	mWalls[Wall::BOTTOM].Init(Wall::BOTTOM);

	mWalls[Wall::LEFT].SetTexture(mWallTex, sf::IntRect{ 0,0,(int)(GDC::SCREEN_RES.x*GDC::WALL_WDITH), (int)(GDC::SCREEN_RES.y*(1- GDC::WALL_WDITH*2)) });
	mWalls[Wall::LEFT].SetPos(Vec2f{ 0,GDC::SCREEN_RES.y*GDC::WALL_WDITH });
	mWalls[Wall::LEFT].SetOrigin(Vec2f{ 0,0 });
	mWalls[Wall::LEFT].Init(Wall::LEFT);

	mWalls[Wall::RIGHT].SetOrigin(Vec2f{ 0,0 });
	mWalls[Wall::RIGHT].SetPos(Vec2f{ GDC::SCREEN_RES.x*(1- GDC::WALL_WDITH),GDC::SCREEN_RES.y*GDC::WALL_WDITH });
	mWalls[Wall::RIGHT].SetTexture(mWallTex, sf::IntRect{ 0,0,(int)(GDC::SCREEN_RES.x*GDC::WALL_WDITH), (int)(GDC::SCREEN_RES.y*(1- GDC::WALL_WDITH*2)) });
	mWalls[Wall::RIGHT].Init(Wall::RIGHT);

}

void PlayMode::Update()
{
	assert(mpGame);

	mGun.Update();

	for (int i = 0; i < Wall::MAX_WALLS; ++i)
		mWalls[i].Update();
}

void PlayMode::Render()
{
	//the instructions
	assert(mpGame);
	GD& gd = mpGame->GetData();
	string mssg = gd.playerName;
	mssg += ", press left or right arrow to spin, space to fire, ESC to quit";
	sf::Text txt(mssg, gd.font, 20);
	txt.setPosition(GDC::SCREEN_RES.x*0.15f, GDC::SCREEN_RES.y*0.15f);
	gd.pWindow->draw(txt);

	mGun.Render(*gd.pWindow);

	for (int i = 0; i < Wall::MAX_WALLS; ++i)
		mWalls[i].Render(*gd.pWindow);
}

void Gun::Update() 
{
	SetPos(Vec2f{ GDC::SCREEN_RES.x / 2.f, GDC::SCREEN_RES.y / 2.f });

	if (mWaitSecs <= 0)
	{
		//here we don't want windowsOS keys, with initial delays and repeat rate delays and all that rubbish
		//so just access the keyboard directly for instant response
		float inc = 0;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			inc = -GDC::PLAY_SPIN_SPD;
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			inc = GDC::PLAY_SPIN_SPD;
		//a bit of physics degress per seconds X seconds for smooth predictable rate of rotation
		SetDegrees(GetDegrees() + inc * Application::GetElapsedSecs());
	}
	else
		mWaitSecs -= Application::GetElapsedSecs();

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		//fire the ball here

		mWaitSecs = GDC::FIRE_DELAY;
	}
}



void GameObj::Update() 
{
	if (mVel.x != 0 || mVel.y != 0)
	{
		sf::Vector2f pos{ mSprite.getPosition() };
		pos += mVel * Application::GetElapsedSecs();
		mSprite.setPosition(pos);
	}
}

inline void Wall::Update()
{
	//add ball collision code here
}
