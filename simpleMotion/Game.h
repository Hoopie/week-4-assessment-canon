#pragma once
/*
Mini game example demonstrates static binding - compile time inheritance and
introduces some simple physics to get things moving. For students to modify
*/


#include <string>

#include "SFML/Graphics.hpp"

//dimensions in 2D that are whole numbers
struct Vec2i
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Vec2f
{
	float x, y;
};

const float PI = 3.14159265358979323846f;
inline float Deg2Rad(float deg) {
	return deg * (PI/180.f);
}

/*
Cannon, walls and ball are all GameObjs
Here we've hidden how we are storing position and make it appear to
be a Vec2f (our type), when in fact it's a sf::Vector2f inside the sprite
We've hidden the use of the sprite.
Now - you might say, you didn't need to bother, perhaps so, but it demonstrates
the technique and means this code could be changed to a different sprite/pos system
easily.
*/
class GameObj
{
public:
	GameObj() : mVel{ 0, 0 } {};
	void Update();
	void Render(sf::RenderWindow& window) {
		window.draw(mSprite);
	}
	void SetTexture(const sf::Texture& t) {
		mSprite.setTexture(t, true);
	}
	void SetTexture(const sf::Texture& t, const sf::IntRect& rect) {
		mSprite.setTexture(t);
		mSprite.setTextureRect(rect);
	}
	void SetOrigin(const Vec2f& off) {
		mSprite.setOrigin(sf::Vector2f(off.x, off.y));
	}
	void SetScale(const Vec2f& s) {
		mSprite.setScale(s.x, s.y);
	}
	Vec2f GetScale() {
		return Vec2f{ mSprite.getScale().x, mSprite.getScale().y };
	}
	Vec2f GetPos() {
		return Vec2f{ mSprite.getPosition().x,mSprite.getPosition().y };
	}
	void SetPos(const Vec2f& pos) {
		mSprite.setPosition(pos.x, pos.y);
	}
	float GetDegrees() {
		return mSprite.getRotation();
	}
	void SetDegrees(float angle) {
		mSprite.setRotation(angle);
	}
	void AddRotation(float angle) {
		mSprite.rotate(angle);
	}
	void SetVel(const Vec2f& v) {
		mVel.x = v.x;
		mVel.y = v.y;
	}
	const Vec2f GetVel() const{
		return Vec2f{ mVel.x,mVel.y };
	}
	Vec2f GetDim() const {
		return Vec2f{ (float)mSprite.getTextureRect().width, (float)mSprite.getTextureRect().height };
	}

private:
	sf::Sprite mSprite; //image and position
	sf::Vector2f mVel;	//velocity
};

//a namespace is just another type of box with a label where we can group things together
//GDC="GameDataConstants" - a box for all your magic numbers
//especially those that a designer might need to tweak to balance the game
namespace GDC
{
	const Vec2i SCREEN_RES{ 1200,800 };	//desired resolution
	const char ESCAPE_KEY{ 27 };		//ASCII code
	const char ENTER_KEY{ 13 };
	const sf::Uint32 ASCII_RANGE{ 127 };//google an ASCII table, after 127 we don't care
	const float PLAY_SPIN_SPD{ 200.f };	//how fast to spin
	const float WALL_WDITH{ 0.1f };		//the walls skirt the edge of the screen
	const float GUN_LENGTH{ 70.f };		//from the centre to the end of the barrel
	const float BALL_SPEED{ 400.f };	//ball speed in units per sec
	const float FIRE_DELAY{ 0.25f };	//stop on firing for this time in secs
	const float CANNON_ROT_OFFSET{ 115 };	//when rotating the cannon, the barrel points at a specific angle (rotate anti-clockwise from horizontal)
}

//keep important shared information "GameData" in one accessible object and pass it around
//note - structs are like classes but everything is always public, so best kept just for data
struct GD
{
	sf::Font font;				//a shared font to use everywhere
	sf::RenderWindow *pWindow;	//can't render anything without this
	std::string playerName;			//the player's name is needed by different objects
};

class Game;

//in this mode we want the player's name, like a game's frontend menu screens
class NameMode
{
public:
	NameMode() :mpGame(nullptr) {}
	//setup
	void Init(Game*);
	//handle any logic
	void Update() {};
	//display
	void Render();
	//process windows text messages
	void TextEntered(char);
private:
	Game *mpGame;	//the only way to communicate with the rest of the game
};


//cannon spins around and can fire one cannon ball
class Gun : public GameObj
{
public:
	Gun() 
		:GameObj(), mWaitSecs(0)
	{}
	void Update();
private:
	float mWaitSecs;	//delay after firing beforeo you can move/fire
};

//cannon balls bounce off walls
class Wall : public GameObj
{
public:
	static const int MAX_WALLS = 4;				//a wall on each side of the screen
	enum WallType { LEFT, RIGHT, TOP, BOTTOM };	//identify where each wall is

	Wall()
		:GameObj(), mType(LEFT)
	{}
	void Init(WallType wtype) {
		mType = wtype;
	}
	void Update();
private:
	WallType mType; //where does this instance fit on screen
};

//in this mode the player can spin the cannon, like a game's playable part
//similar structure to the NameMode, imagine more of these objects for each
//significant part of the game, all structured the same way
class PlayMode
{
public:
	PlayMode() : mpGame(nullptr) {}
	void Init(Game*);
	void Update();
	void Render();
private:
	Game *mpGame; //for communication

	sf::Texture mCannonTex;			//cannon and ball
	sf::Texture mWallTex;			//walls
	Wall mWalls[Wall::MAX_WALLS];	//four walls
	Gun mGun;		//cannon
};


//manage the game's different behaviours
class Game
{
public:
	//control what the game is doing 
	enum class StateMachine {
		WAITING_INIT,	//not in a safe state yet
		GET_NAME,		//what's your name?
		PLAY			//let's play!!
	};

	Game();
	//setup
	void Initialise(sf::RenderWindow&);
	//logic
	void Update();
	//display
	void Render();
	//input from windows
	void TextEntered(char);

	//accessors
	StateMachine& GetState() { return mState; }
	void SetState(StateMachine nState) { mState = nState; }

	GD& GetData() { return mGD; }
private:
	GD mGD;				//shared game data, be careful here we are breaking rules of encapsulation
	StateMachine mState;//contols what's going on in the 
	PlayMode mPMode;	//object used for playing the game (like a mini game)
	NameMode mNMode;	//object used for getting the player's name (like a mini frontend)
};

